# Projet (algorithmique Avancée) - PECQUEUX Théo

## Paramétre

* Paramètres a définir a chaque éxecution:
  * **nb_disques** : nombre de disques 
  * **nb_towers** : nombre de tours
  * **nb_etape** : nombre d'étape maximum
* Paramètres défini a partir des entrées:
  * **start** : start[i] -> tours du disque i à l'étape initial
  * **end** : end[i] -> tours du disque i à l'étape final
  * **hStart** : hStart[i] -> hauteur du disque i à l'étape initial
  * **hEnd** : hEnd[i] -> hauteur du disqu i à l'étape final

## Variables

* Coup jouer a chaque étape:
  * **move** : move[i] -> disques déplacé à l'étape i
  * **pose** : pose[i] -> tours sur la quelle est placé le disque à l'étape i
* listes de toutes les tours et de tous les disques(hauteur du disques et tour du disque):
  * **status** : status[i,j] -> numéro de tours du disque i à l'étape j
  * **height** : height[i,j] -> hauteur du disque i à l'étape j
  * **towers** : towers[i,j] -> liste des disques de la tour i à l'étape j

## algorithm

Afin de pouvoir dérouler l'algorithm, il faudrait une boucle afin de pouvoir parcourir les n étapes et de pouvoir calculer d'abord les tableaux move et pose et ensuite de mettre à jour les 3 autres tableaux (status, height et towers) grâce au 2 premiers
